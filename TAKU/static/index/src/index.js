$(function() {
	var oWidth = $('.j_item').width();
	var len = $('.j_item').length;
	var oGap = parseInt($('.j_item').css('marginLeft'));
	var mLeft = Math.abs(parseInt($('.j_cont').css('marginLeft')));
	var totalWidth = oWidth * len + oGap * len;
	var num = len % 4;
	var oLeft = mLeft;

	if(num == 0){
		num = 4;
	}
	
	var max = totalWidth - oWidth * num - oGap * (num - 1);
	var min = oGap;

	$('.j_cont').css('width',  totalWidth + 'px');

	$('.j_next').on('click', function(){

		if(oLeft < max) {

			oLeft = oLeft + oWidth * 4  + oGap * 4 ;

			$('.j_cont').animate({
				'marginLeft': - oLeft + 'px'
			});
		} else {
			oLeft =  max;
		}
	});

	$('.j_prev').on('click', function(){
		

		if(oLeft > min) {

			oLeft = oLeft - oWidth * 4 - oGap * 4;

			$('.j_cont').animate({
				'marginLeft': - oLeft + 'px'
			});	
		} else {
			oLeft = min;
		}
		
	});
});